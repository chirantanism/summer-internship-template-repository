## Team Name - Project Name

The participants are required to clone this repository and create a private Gitlab repository under their own username (Single repository per team). The following created sections in this README.md need to be duly filled, highlighting the denoted points for the solution/implementation. Please feel free to create further sub-sections in this markdown, the idea is to understand the gist of the components in a singular document.

### Project Overview
----------------------------------

A brief description of 
* What problem did the team try to solve
* What is the proposed solution

### Solution Description
----------------------------------

#### Architecture Diagram

Affix an image of the flow diagram/architecture diagram of the solution

#### Technical Description

An overview of 
* What technologies/versions were used
* Setup/Installations required to run the solution
* Instructions to run the submitted code

### Team Members
----------------------------------

List of team member names and email IDs with their contributions
Brute_Force-Supply Chain Buddy

Problem:We are trying to solve the problem of traditional learning of the members of an organization.

Solution:We are automating the learning of the newcomers of an organization by building an app which can explain the workflow of the organization in an interactive way.

Solution Description:We are creating a mixed reality based unity app in whch the user just clicks the holograms inside a virtual environment
 to gather information about a particular concept.

Architecture diagram:
Virtual Environment ->User interacts with the virtual environment and learns -> Solve their own technical issues

Technical description-
Windows 10 
Windows 10 SDK(10.018362.0)
Hololens(1st gen) emulator
Visual studio 2019
Unity 2018.4.x

Technologies and Tools used:
C#,Mixed Reality Toolkit,Unity,Visual Studio 2019

Team Members:
Meghmala Sahu(B417015)-integating the application with with universal windows platform.
Ayush Chamoli(B417012)-writing the code for taking input from system in unity.
Chirantan Mohanty(b417013)-making the unity mixed reality application.
Radhasharana Pradhan(B417031)-making the unity mixed reality application.